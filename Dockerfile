FROM ubuntu
WORKDIR /root

RUN apt update && apt -y upgrade
RUN apt -y install curl git zip unzip php7.0 php7.0-curl php7.0-mbstring php7.0-dom php7.0-pdo php7.0-pdo-sqlite php7.0-pdo-mysql

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

# Install nodejs and npm
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt -y install nodejs